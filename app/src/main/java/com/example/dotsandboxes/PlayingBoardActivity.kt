package com.example.dotsandboxes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class PlayingBoardActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_playing_board)
    }
}
